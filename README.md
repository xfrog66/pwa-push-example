# PwaPushExample
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.


## Description
This sample of app is a PoC to investigate how PWA and Web push can work together. This is composed by

- an angular front-end that will register the browser to recieve the notification and can also be used to send notification to all the suscribed clients
- a node-express backend to recieve the subscriptions and also trigger the notification. 

The server does not have any database. Just storing the subscription in memory for the demo purpose.

Once subscriptions are done, you can also trigger the notifications using the following curl action (or using postman)
```
curl -X POST \
  http://localhost:5000/notify \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"notification": {
		"title": "Sample of title",
		"body": "Sample of body"
	}
}'
```


## Build
Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Start the server
After running build, run `npm run start:prod`. This will both start a web-server to serve the angular application and a node-express server to simulate the backend.
