const express = require('express');
const cors = require('cors')
const webPush = require('web-push');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
app.use(bodyParser.json());

const publicVapidKey = "BPVhpNunI6s5l2-g8fRJ0rZ0VKp8DROqkknYn42EjcqTFEV3Lgj2MwBRHdoM4Hf6V9zBCas_47medh576-7fgzs";
const privateVapidKey = "BfvOglMuep2Ki5x8CtUjP6-GuxTshz1EnIVuoI67EBk";


let subscriptions = [];

webPush.setVapidDetails('mailto:sender@example.com', publicVapidKey, privateVapidKey);
app.post('/subscribe', (req, res) => {
  const subscription = req.body.notification;

  subscriptions.push(subscription);


  subscriptions = subscriptions.filter((subscription,index) => {
    return index === subscriptions.findIndex(obj => {
      return JSON.stringify(obj) === JSON.stringify(subscription);
    });
  });

  
  console.log(`Subscription received`);
  res.status(201).json({});
  
});



app.post('/notify', (req, res) => {

  const notification = req.body.notification;

  console.log(`Start notification`);
  
  const payload = JSON.stringify({
    notification: {
      title: notification.title,
      body: notification.body,
      icon: 'https://www.shareicon.net/data/256x256/2015/10/02/110808_blog_512x512.png',
      vibrate: [100, 50, 100],
      data: {
        url: "http://www.google.fr"
      }
    }
  });

  console.log(payload);
  

  for (const subscription of subscriptions) {
    webPush.sendNotification(subscription, payload)
    .catch(error => console.log('failed to send to ' + subscription.endpoint));  
  }

  
  res.status(201).json(subscriptions);

});



app.set('port', process.env.PORT || 5000);
const server = app.listen(app.get('port'), () => {
  console.log(`Express running → PORT ${server.address().port}`);
});