import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebNotificationService {

  readonly VAPID_PUBLIC_KEY = 'BPVhpNunI6s5l2-g8fRJ0rZ0VKp8DROqkknYn42EjcqTFEV3Lgj2MwBRHdoM4Hf6V9zBCas_47medh576-7fgzs';
  private baseUrl = 'http://localhost:5000/subscribe';
  private notifyUrl = 'http://localhost:5000/notify';
  constructor(private http: HttpClient,
              private swPush: SwPush) {}
  subscribeToNotification() {
    this.swPush.requestSubscription({
        serverPublicKey: this.VAPID_PUBLIC_KEY
    })
    .then(sub => this.subscribeToServer(sub))
    .catch(err => console.error('Could not subscribe to notifications', err));
  }

  subscribeToServer(params: any) {
    this.http.post(this.baseUrl, { notification : params }).subscribe();
  }


  triggerNotification(params: any) {
    this.http.post(this.notifyUrl, { notification : params }).subscribe();
  }




}
