import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WebNotificationService } from './web-notification.service';
import { SwPush } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'pwa-push-example';
  isEnabled = this.swPush.isEnabled;
  isGranted = Notification.permission === 'granted';

  @ViewChild('titleInput') titleInput: ElementRef;


  constructor(private swPush: SwPush,
    private webNotificationService: WebNotificationService) {}


  ngOnInit(): void {
    if (this.isEnabled) {
      this.webNotificationService.subscribeToNotification();
    }
  }


  submitNotification(): void {

    const notif = {
      title: this.titleInput.nativeElement.value,
      body: new Date().toISOString()
    };

    console.log(notif);

    this.webNotificationService.triggerNotification(notif);
  }




}
